// Import the functions you need from the SDKs you need

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

// TODO: Add SDKs for Firebase products that you want to use
import {getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import {getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

// https://firebase.google.com/docs/web/setup#available-libraries


const firebaseConfig = {

    apiKey: "AIzaSyD4eOGFSrMYrAN7d4sve-AnEhcbOg3ENjU",

    authDomain: "proyectofinal-95587.firebaseapp.com",

    databaseURL: "https://proyectofinal-95587-default-rtdb.firebaseio.com",

    projectId: "proyectofinal-95587",

    storageBucket: "proyectofinal-95587.appspot.com",

    messagingSenderId: "726144861416",

    appId: "1:726144861416:web:d8c3a56113a41d3ad6f55d"

  };




// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

/* Productos */

    window.onload = mostrarProductos(); 

var productos = document.getElementById("contenidoP");
var btnProductos = document.getElementById("btnActualizarProductos");

// mostrar datos

function mostrarProductos(){
    if(productos != null){
        productos.innerHTML = "";
    }
    const dbref = ref(db, 'productos/');
    onValue(dbref, (snapshot)=>{
        snapshot.forEach(childSnapshot => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();
        if(productos){
            if(childData.estado == 0){
                productos.innerHTML = productos.innerHTML+
                "<div class='imagenes'><img src='"+childData.url+"'><br><h4>"+childData.nombre+
                "</h4><div class='textoE'><h3>"+childData.descripcion+"</h3><h3>Precio: $"+childData.precio+
                "</h3></div></div>"
            }
        }                                     
        });
    },{
        onlyOnce: true
    });

}

function mostrarProductosAdmin(){
    if(productos != null){
        productos.innerHTML = "";
    }
    const dbref = ref(db, 'productos/');
    onValue(dbref, (snapshot)=>{
        snapshot.forEach(childSnapshot => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();
        if(productos){
            if(childData.estado == 0){
                productos.innerHTML = productos.innerHTML+
                "<div class='imagenes'><img src='"+childData.url+"'><br><h4>"+childData.nombre+
                "</h4><div class='textoP'><h3>"+childData.descripcion+"</h3><h3>Precio: $"+childData.precio+
                "</h3></div></div>"
            }
        }                                     
        });
    },{
        onlyOnce: true
    });

}


