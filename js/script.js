// Import the functions you need from the SDKs you need

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

// TODO: Add SDKs for Firebase products that you want to use
import {getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import {getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

// https://firebase.google.com/docs/web/setup#available-libraries


const firebaseConfig = {

    apiKey: "AIzaSyD4eOGFSrMYrAN7d4sve-AnEhcbOg3ENjU",

    authDomain: "proyectofinal-95587.firebaseapp.com",

    databaseURL: "https://proyectofinal-95587-default-rtdb.firebaseio.com",

    projectId: "proyectofinal-95587",

    storageBucket: "proyectofinal-95587.appspot.com",

    messagingSenderId: "726144861416",

    appId: "1:726144861416:web:d8c3a56113a41d3ad6f55d"

  };




// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getDatabase();



/* Variables de botones */

var btnGuardar = document.getElementById("btnGuardar");
var btnActualizar = document.getElementById("btnActualizar");
var btnConsultar = document.getElementById("btnConsultar");
var btnDeshabilitar = document.getElementById("btnDeshabilitar");
var btnLimpiar = document.getElementById("btnLimpiar");
var btnVerImagen = document.getElementById("verImagen");
var archivo = document.getElementById("archivo");




// Insertar
var codigo = "";
var nombre = "";
var descripcion = "";
var precio = "";
var imgNombre = "";
var estado = 1;
var url = "";
var imgNombre = "";


function leerInputs(){
    codigo = document.getElementById("codigo").value;
    nombre = document.getElementById("nombre").value;
    descripcion = document.getElementById("descripcion").value;
    precio = document.getElementById('precio').value;
    imgNombre = document.getElementById("imgNombre").value;
    url = document.getElementById("url").value;
}

function insertDatos(){
  leerInputs();

  set(ref(db,'productos/' + codigo),{
      nombre: nombre,
      descripcion:descripcion,
      precio:precio,
      imgNombre:imgNombre,
      url:url,
      estado:0
      /* poner el nombre y la url*/
  })
  .then((docRef) => {
      alert("Se ha registrado el producto exitosamente.");
  })
  .catch((error) => {
      alert("Ha ocurrido un error al registrar el producto.")
  });
}

function cargarImagen(){
  const file = event.target.files[0];
  const name = event.target.files[0].name;
  
  const storage = getStorage();
  const storageRef = refS(storage, 'imagenes/' + name);

  uploadBytes(storageRef, file).then((snapshot) => {
      document.getElementById('imgNombre').value = name;
      alert('Se cargo el archivo.');
  });
}

function descargarImagen(){
  archivo = document.getElementById('imgNombre');


  // Create a reference to the file we want to download
  const storage = getStorage();
  const starsRef = refS(storage, 'imagenes/'+archivo.value);

  // Get the download URL
  getDownloadURL(starsRef)
  .then((url) => {
      // Insert url into an <img> tag to "download"
      document.getElementById('url').value=url;
      const img = document.getElementById('img');
      img.setAttribute('src', url);
  })
  .catch((error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
      case 'storage/object-not-found':
          console.log("No existe el archivo.")
          break;
      case 'storage/unauthorized':
          // User doesn't have permission to access the object
          console.log("No tienes permisos.")
          break;
      case 'storage/canceled':
          // User canceled the upload
          console.log("La subida se ha cancelado.")
          break;

      // ...

      case 'storage/unknown':
          // Unknown error occurred, inspect the server response
          console.log("Un error inesperado ha ocurrido.")
          break;
      }

      
  });
}

function actualizar(){
    leerInputs();
    update(ref(db,'productos/'+ codigo),{
        nombre: nombre,
        descripcion:descripcion,
        precio:precio,
        imgNombre:imgNombre,
        url:url
    }).then(()=>{
        alert("Se actualizaron los datos.");
    })
    .catch(()=>{
        alert("Ocurrio un error");
    });
}

function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);
        get(child(dbref,'productos/'+ codigo)).then((snapshot)=>{
            if(snapshot.val().estado==0){
            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            imgNombre = snapshot.val().imgNombre;
            url = snapshot.val().url;
            console.log(snapshot.val().estado);
            escribirInputs();
        }
        
        else{
            alert("No existe el registro.");
        }
        }).catch((error)=>{
            alert("Error al buscar.");
        });
}

function escribirInputs(){
    document.getElementById('codigo').value=codigo;
    document.getElementById('nombre').value= nombre;
    document.getElementById('descripcion').value= descripcion;
    document.getElementById('precio').value= precio;
    document.getElementById('url').value= url;
    document.getElementById('imgNombre').value= imgNombre;
}

function deshabilitar(){
    leerInputs();
    update(ref(db,'productos/'+ codigo),{
        estado:1
    }).then(()=>{
        alert("Se ha borrado el registro.");
    })
    .catch(()=>{
        alert("Ocurrio un error");
    });
}


function limpiar(){
    document.getElementById("codigo").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("descripcion").value = "";
    document.getElementById('precio').value = "";
    document.getElementById("imgNombre").value = "";
    document.getElementById("url").value = "";
    document.getElementById("archivo").value = "";
    document.getElementById("img").src = "";
    
    alert("Se ha limpiado con exito.");
}

btnGuardar.addEventListener('click',insertDatos);
archivo.addEventListener('change',cargarImagen);
btnActualizar.addEventListener('click',actualizar);
btnConsultar.addEventListener('click',mostrarDatos);
btnVerImagen.addEventListener('click',descargarImagen);
btnDeshabilitar.addEventListener('click',deshabilitar);
btnLimpiar.addEventListener('click',limpiar);







